﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TextAdventure.Game.Objects;

namespace TextAdventure.Utility
{
    public static class ItemIdMapper
    {
        public static BaseSystem.Base.Item ConstructItemByID(string id)
        {
            Type type = Type.GetType(id);
            return (BaseSystem.Base.Item)Activator.CreateInstance(type);
        }
    }
}
