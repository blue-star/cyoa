﻿using System.Collections.Generic;

namespace TextAdventure.BaseSystem.Base
{
    public class ItemSaveEntry
    {
        public string ItemType;
        public ItemSaveDataContainer SaveData;
    }

    public class ItemSaveDataContainer
    {
        public List<ItemSaveString> Strings = new List<ItemSaveString>();
        public List<ItemSaveBool> Bools = new List<ItemSaveBool>();
        public List<ItemSaveInt> Ints = new List<ItemSaveInt>();
        public List<ItemSaveDouble> Doubles = new List<ItemSaveDouble>();

        public void SetString(string key, string data)
        {
            ItemSaveString entry = Strings.Find(o => o.Key == key);

            if (entry == null)
            {
                entry = new ItemSaveString();
                entry.Key = key;

                Strings.Add(entry);
            }

            entry.Value = data;
        }

        public void SetBool(string key, bool data)
        {
            ItemSaveBool entry = Bools.Find(o => o.Key == key);

            if (entry == null)
            {
                entry = new ItemSaveBool();
                entry.Key = key;

                Bools.Add(entry);
            }

            entry.Value = data;
        }

        public void SetDouble(string key, double data)
        {
            ItemSaveDouble entry = Doubles.Find(o => o.Key == key);

            if (entry == null)
            {
                entry = new ItemSaveDouble();
                entry.Key = key;

                Doubles.Add(entry);
            }

            entry.Value = data;
        }

        public void SetInt(string key, int data)
        {
            ItemSaveInt entry = Ints.Find(o => o.Key == key);

            if (entry == null)
            {
                entry = new ItemSaveInt();
                entry.Key = key;

                Ints.Add(entry);
            }

            entry.Value = data;
        }

        public string GetString(string key)
        {
            ItemSaveString entry = Strings.Find(o => o.Key == key);
            return entry != null? entry.Value : "";
        }

        public bool GetBool(string key)
        {
            ItemSaveBool entry = Bools.Find(o => o.Key == key);
            return entry != null ? entry.Value : false;
        }

        public double GetDouble(string key)
        {
            ItemSaveDouble entry = Doubles.Find(o => o.Key == key);
            return entry != null ? entry.Value : 0;
        }

        public int GetInt(string key)
        {
            ItemSaveInt entry = Ints.Find(o => o.Key == key);
            return entry != null ? entry.Value : 0;
        }

        public class ItemSaveString
        {
            public string Key;
            public string Value;
        }

        public class ItemSaveInt
        {
            public string Key;
            public int Value;
        }

        public class ItemSaveBool
        {
            public string Key;
            public bool Value;
        }

        public class ItemSaveDouble
        {
            public string Key;
            public double Value;
        }
    }
}
