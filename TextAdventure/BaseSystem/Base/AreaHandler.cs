﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextAdventure.BaseSystem.Base
{
    public class AreaHandler
    {
        private List<Area> areas = new List<Area>();

        public Area GetAreaByID(int id)
        {
            return areas[id];
        }

        public Area FindItemOwner(Item item)
        {
            return areas.Find(o => o.Items.Contains(item));
        }

        public T FindAreaOfType<T>() where T : Area
        {
            Area area = areas.Find(o => o is T);

            if(area == null)
            {
                return default(T);
            }

            return (T)area;
        }

        public void InitializeAreas()
        {
            //create instances
            Area testArea = CreateArea(new Game.Areas.TestArea());

            Area otherLand = CreateArea(new Game.Areas.TheOtherLand());

            Area hallway = CreateArea(new Game.Areas.Hallway());

            Area livingRoom = CreateArea(new Game.Areas.LivingRoom());

            Area wc = CreateArea(new Game.Areas.WC());

            Area kitchen = CreateArea(new Game.Areas.Kitchen());

            Area upperHallway = CreateArea(new Game.Areas.UpperHallway());

            Area bathroom = CreateArea(new Game.Areas.Bathroom());

            Area bedroom1 = CreateArea(new Game.Areas.Bedroom1());

            Area bedroom2 = CreateArea(new Game.Areas.Bedroom2());

            Area largeBedroom = CreateArea(new Game.Areas.LargeBedroom());

            Area walkInCloset = CreateArea(new Game.Areas.WalkInCloset());

            Area attic = CreateArea(new Game.Areas.Attic());

            Area basement = CreateArea(new Game.Areas.Basement());

            SetAreaID();
            ConnectAreas();
        }

        public void ConnectAreas()
        {
            for (int i = 0; i < areas.Count; i++)
            {
                areas[i].SetupConnections();
            }
        }

        public Area CreateArea(Area area)
        {
            areas.Add(area);
            return area;
        }

        public void SetAreaID()
        {
            for (int i = 0; i < areas.Count; i++)
            {
                areas[i].AreaID = i;
            }
        }

        public void LoadAreaData()
        {
            for(int i = 0; i < areas.Count; i++)
            {
                areas[i].Load();
            }
        }

        public void SaveAreaData()
        {
            for (int i = 0; i < areas.Count; i++)
            {
                areas[i].Save();
            }
        }
    }
}
