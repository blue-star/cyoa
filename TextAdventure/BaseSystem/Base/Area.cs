﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextAdventure.BaseSystem.Base
{
    public abstract class Area
    {
        public List<Item> Items = new List<Item>();
        public int AreaID = -1;
        
        public List<Direction> Connections = new List<Direction>();
        
        public abstract void Save();
        public abstract void Load();

        public abstract string GetLookDescription();
        public abstract void SetupConnections();

        protected AreaItemData GetItemSaveData()
        {
            AreaItemData data = new AreaItemData();
                 
            data.Items = new ItemSaveEntry[Items.Count];
            data.Connections = Connections.ToArray();

            for (int i = 0; i < Items.Count; i++)
            {
                data.Items[i] = new ItemSaveEntry();
                data.Items[i].ItemType = Items[i].GetItemID();
                data.Items[i].SaveData = Items[i].SaveData;
            }

            return data;
        }

        protected void ApplyItemSaveData(AreaItemData data)
        {
            Items = new List<Item>();

            if (data.Connections != null)
            {
                for (int i = 0; i < data.Connections.Length; i++)
                {
                    Direction dir = Connections.Find(o => o.ConnectedAreaID == data.Connections[i].ConnectedAreaID);

                    if (dir != null)
                    {
                        dir.Availability = data.Connections[i].Availability;
                    }
                }
            }

            for (int i = 0; i < data.Items.Length; i++)
            {
                Item item = Utility.ItemIdMapper.ConstructItemByID(data.Items[i].ItemType);

                item.SaveData = data.Items[i].SaveData;

                Items.Add(item);
            }
        }

        public void ConnectTo(string name, Area area, string description, bool hidden = false)
        {
            Direction direction = new Direction();

            direction.Name = name;
            direction.ConnectedAreaID = area.AreaID;
            direction.Description = description;
            direction.Availability = hidden? Direction.DirectionStates.Hidden : Direction.DirectionStates.Available;

            Connections.Add(direction);
        }

        public void ConnectToLocked(string name, Area area, string description, string lockedMessage)
        {
            Direction direction = new Direction();

            direction.Name = name;
            direction.ConnectedAreaID = area.AreaID;
            direction.Description = description;
            direction.Availability = Direction.DirectionStates.Locked;
            direction.LockedMessage = lockedMessage;

            Connections.Add(direction);
        }

        public class AreaItemData
        {
            public ItemSaveEntry[] Items;
            public Direction[] Connections;
        }

        public class Direction
        {
            public string Name;
            public int ConnectedAreaID;
            public string Description;
            public DirectionStates Availability;
            public string LockedMessage = "This direction is not accessible.";

            public override string ToString()
            {
                return Name;
            }

            public enum DirectionStates
            {
                Available,
                Hidden,
                Locked
            }
        }
    }
}
