﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextAdventure.BaseSystem.Base
{
    public abstract class Item
    {
        public abstract bool GetCanPickUp();
        public abstract bool GetCanDrop();
        public abstract bool GetCanActivate();

        public abstract string GetName();
        public abstract string GetShortDescription();
        public abstract void DoExamineDescription();

        public abstract void DoActivateItem();

        public abstract void UseOnItem(Item other);

        public ItemSaveDataContainer SaveData = new ItemSaveDataContainer();

        public override string ToString()
        {
            return GetName();
        }

        public string GetItemID()
        {
            return GetType().ToString();
        }

        public Area CurrentArea
        {
            get
            {
                return MainWindow.Instance.CurrentArea;
            }
        }

        public void DestroyItem()
        {
            if(MainWindow.Instance.Player.Inventory.Contains(this))
            {
                MainWindow.Instance.Player.Inventory.Remove(this);
            }

            Area itemArea = MainWindow.Instance.AreaHandler.FindItemOwner(this);

            if (itemArea != null)
                itemArea.Items.Remove(this);

            Actor itemOwner = MainWindow.Instance.ActorHandler.FindItemOwner(this);

            if (itemOwner != null)
                itemOwner.Inventory.Remove(this);

            MainWindow.Instance.RefreshAll();
        }
    }
}
