﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TextAdventure.BaseSystem.Base;
using TextAdventure.Game.Objects;

namespace TextAdventure.Game.Actors
{
    public class TestMan : BaseSystem.Base.Actor
    {
        public TestmanSaveData data = new TestmanSaveData();

        public override void DoExamineDescription()
        {
            MainWindow.Instance.WriteLineExamine("The man is wearing a light brown coat, and a hat. The hat seems to be missing the left half, and his shoes are different colors. " + (data.Alive? "He's holding a big bag filled with screwdrivers." : "He is dead."));
        }

        public override void DoDialogue()
        {
            if (data.Alive)
            {
                Speak("Hello, I am Testman. I collect screwdrivers.");
                Known = true;
            }
            else
            {
                if(Known)
                {
                    MainWindow.Instance.WriteLine(GetName() + " is dead. He cannot speak.");
                }
                else
                {
                    MainWindow.Instance.WriteLine("The corpse does not speak.");
                }
            }
        }

        public override string GetNameKnown()
        {
            return "Testman" + (data.Alive? "" : "'s corpse");
        }

        public override string GetNameUnknown()
        {
            return "Some" + (data.Alive? "" : " dead") +" guy";
        }

        public override string GetShortDescription()
        {
            return "A strange person. Seems shady, somehow." + (data.Alive? "" : " Dead.");
        }

        public override void LoadData()
        {
            if (DataSaver.FileExists("TestMan"))
            {
                data = DataSaver.LoadFile<TestmanSaveData>("TestMan");

                ApplyActorData(data.ItemData);
            }
        }

        public override void SaveData()
        {
            data.ItemData = GetActorData();

            data.SaveAsFile("TestMan");
        }

        public override bool TryReceiveItem(Item item)
        {
            if (data.Alive)
            {
                if (item is Screwdriver)
                {
                    Speak("Thanks! I'm collecting these.");
                    Inventory.Add(item);
                    return true;
                }

                Speak("Thanks, but I don't need that " + item.GetName());
                return false;
            }
            else
            {
                MainWindow.Instance.WriteLine("You place the " + item.GetName() + " next to his corpse.");

                CurrentArea.Items.Add(item);

                return true;
            }
        }

        public override void UseItemOnActor(Item item)
        {
            if (data.Alive)
            {
                if (item is Faucet)
                {
                    Speak("Could you not prod me with that " + item.GetName() + "?");
                }

                if (item is Screwdriver)
                {
                    if (Known)
                    {
                        MainWindow.Instance.WriteLine("In a cruel twist of fate, " + GetName() + " was murdered with the item he held most dear. As the screwdriver pierced his lungs, you could feel " + GetName() + " taking his last breath. He drops his bag of screwdrivers, which falls over and spills his contents much like " + GetName() + "'s blood is spilled by you. He is dead now.");
                    }
                    else
                    {
                        MainWindow.Instance.WriteLine("In a spur of random violence, you stab the man with the screwdriver. He looks surprised, and eventually succumbs to his wound. As he drops his bag, several screwdrivers fall out. He is dead now.");
                    }

                    CurrentArea.Items.Add(new Screwdriver());
                    CurrentArea.Items.Add(new Screwdriver());
                    CurrentArea.Items.Add(new Screwdriver());
                    CurrentArea.Items.Add(new Screwdriver());
                    CurrentArea.Items.Add(new Screwdriver());

                    MainWindow.Instance.RefreshAll();
                    MainWindow.Instance.RefreshAll();

                    data.Alive = false;
                }
            }
            else
            {
                MainWindow.Instance.WriteLine("You prod the " + item.GetName() + " into the corpse. It does nothing but exemplify your cruelty.");
            }
        }

        public class TestmanSaveData
        {
            public bool Alive = true;
            public ActorData ItemData;
        }
    }
}
