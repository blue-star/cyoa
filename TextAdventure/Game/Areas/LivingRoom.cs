﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextAdventure.Game.Areas
{
    public class LivingRoom : BaseSystem.Base.Area
    {
        public override string GetLookDescription()
        {
            return "A tidy living room. The furniture looks worn, but clean.";
        }

        public override void Load()
        {
            if (DataSaver.FileExists("LivingRoom"))
            {
                AreaData save = DataSaver.LoadFile<AreaData>("LivingRoom");

                ApplyItemSaveData(save.ItemData);
            }
        }

        public override void Save()
        {
            AreaData save = new AreaData();

            save.ItemData = GetItemSaveData();

            save.SaveAsFile("LivingRoom");
        }

        public override void SetupConnections()
        {
            ConnectTo("Kitchen", MainWindow.Instance.AreaHandler.FindAreaOfType<Kitchen>(), "A passage leading to the kitchen.");
            ConnectTo("WC", MainWindow.Instance.AreaHandler.FindAreaOfType<WC>(), "A small room containing the toilet.");
            ConnectTo("Hallway", MainWindow.Instance.AreaHandler.FindAreaOfType<Hallway>(), "The hallway, which leads outside.");

            ConnectTo("Staircase up", MainWindow.Instance.AreaHandler.FindAreaOfType<UpperHallway>(), "A staircase going up.");
            ConnectTo("Staircase down", MainWindow.Instance.AreaHandler.FindAreaOfType<Basement>(), "A staircase going down to the basement.");
        }

        public class AreaData
        {
            public AreaItemData ItemData;
        }
    }
}
