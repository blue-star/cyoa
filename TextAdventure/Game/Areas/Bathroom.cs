﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TextAdventure.Game.Objects;

namespace TextAdventure.Game.Areas
{
    public class Bathroom : BaseSystem.Base.Area
    {
        public AreaData areaData = new AreaData();

        public Bathroom()
        {
            Items.Add(new Bathtub());
        }

        public override string GetLookDescription()
        {
            return "A pretty old bathroom. There's a bathtub, which has a shower. The sink seems to have been used often, but not recently.";
        }

        public override void Load()
        {
            if (DataSaver.FileExists("Bathroom"))
            {
                areaData = DataSaver.LoadFile<AreaData>("Bathroom");

                ApplyItemSaveData(areaData.ItemData);
            }
        }

        public override void Save()
        {
            areaData.ItemData = GetItemSaveData();

            areaData.SaveAsFile("Bathroom");
        }

        public override void SetupConnections()
        {
            ConnectTo("Hallway", MainWindow.Instance.AreaHandler.FindAreaOfType<UpperHallway>(), "Back to the hallway.");
        }

        public class AreaData
        {
            public AreaItemData ItemData;
        }
    }
}
