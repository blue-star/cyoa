﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextAdventure.Game.Areas
{
    public class UpperHallway : BaseSystem.Base.Area
    {
        public override string GetLookDescription()
        {
            return "A reasonably-sized hallway. There are a few doors to different rooms.";
        }

        public override void Load()
        {
            if (DataSaver.FileExists("UpperHallway"))
            {
                AreaData save = DataSaver.LoadFile<AreaData>("UpperHallway");

                ApplyItemSaveData(save.ItemData);
            }
        }

        public override void Save()
        {
            AreaData save = new AreaData();

            save.ItemData = GetItemSaveData();

            save.SaveAsFile("UpperHallway");
        }

        public override void SetupConnections()
        {
            ConnectTo("Staircase down", MainWindow.Instance.AreaHandler.FindAreaOfType<LivingRoom>(), "A staircase leading down, back to the living room.");

            ConnectTo("Bathroom", MainWindow.Instance.AreaHandler.FindAreaOfType<Bathroom>(), "A glass door leading to the bathroom.");

            ConnectTo("First Bedroom", MainWindow.Instance.AreaHandler.FindAreaOfType<Bedroom1>(), "A green door to the first bedroom.");

            ConnectTo("Second Bedroom", MainWindow.Instance.AreaHandler.FindAreaOfType<Bedroom2>(), "A pink door to the second bedroom.");

            ConnectTo("Master Bedroom", MainWindow.Instance.AreaHandler.FindAreaOfType<LargeBedroom>(), "An orange door to the master bedroom.");

            ConnectTo("Staircase up", MainWindow.Instance.AreaHandler.FindAreaOfType<Attic>(), "A staircase leading up to the attic.");
        }

        public class AreaData
        {
            public AreaItemData ItemData;
        }
    }
}
