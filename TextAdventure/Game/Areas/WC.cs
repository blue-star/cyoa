﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TextAdventure.Game.Objects;

namespace TextAdventure.Game.Areas
{
    public class WC : BaseSystem.Base.Area
    {
        public WC()
        {
            Items.Add(new Toilet());
        }

        public override string GetLookDescription()
        {
            return "A very small room. There's a toilet here.";
        }

        public override void Load()
        {
            if (DataSaver.FileExists("WC"))
            {
                AreaData save = DataSaver.LoadFile<AreaData>("WC");

                ApplyItemSaveData(save.ItemData);
            }
        }

        public override void Save()
        {
            AreaData save = new AreaData();

            save.ItemData = GetItemSaveData();

            save.SaveAsFile("WC");
        }

        public override void SetupConnections()
        {
            ConnectTo("Living Room", MainWindow.Instance.AreaHandler.FindAreaOfType<LivingRoom>(), "Back to the living room.");
        }

        public class AreaData
        {
            public AreaItemData ItemData;
        }
    }
}
