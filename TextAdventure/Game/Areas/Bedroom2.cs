﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextAdventure.Game.Areas
{
    public class Bedroom2 : BaseSystem.Base.Area
    {
        public override string GetLookDescription()
        {
            return "A small bedroom. There are flowers painted onto the walls. There is a children's bed, along with a desk.";
        }

        public override void Load()
        {
            if (DataSaver.FileExists("Bedroom2"))
            {
                AreaData save = DataSaver.LoadFile<AreaData>("Bedroom2");

                ApplyItemSaveData(save.ItemData);
            }
        }

        public override void Save()
        {
            AreaData save = new AreaData();

            save.ItemData = GetItemSaveData();

            save.SaveAsFile("Bedroom2");
        }

        public override void SetupConnections()
        {
            ConnectTo("Hallway", MainWindow.Instance.AreaHandler.FindAreaOfType<UpperHallway>(), "Back to the hallway");
        }

        public class AreaData
        {
            public AreaItemData ItemData;
        }
    }
}
