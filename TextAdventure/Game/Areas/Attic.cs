﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TextAdventure.Game.Objects;

namespace TextAdventure.Game.Areas
{
    public class Attic : BaseSystem.Base.Area
    {
        private AreaData areaData = new AreaData();
        
        public void OnTurnOnLights()
        {
            areaData.LightsOn = true;
            Items.Add(new Screwdriver());
        }

        public override string GetLookDescription()
        {
            return "The attic is large" + (areaData.LightsOn? "" : ", dark,") + " and covered in dust. It seems nobody has been here in a very long time, but it's not been as well cleaned as the rest of the house." + (areaData.LightsOn? " A single bulb illuminates the room." : " It's hard to see.");
        }

        public override void Load()
        {
            if (DataSaver.FileExists("Attic"))
            {
                areaData = DataSaver.LoadFile<AreaData>("Attic");
            
                ApplyItemSaveData(areaData.ItemData);
            }
        }

        public override void Save()
        {
            areaData.ItemData = GetItemSaveData();

            areaData.SaveAsFile("Attic");
        }

        public override void SetupConnections()
        {
            ConnectTo("Staricase down", MainWindow.Instance.AreaHandler.FindAreaOfType<UpperHallway>(), "A staircase going down, back to the hallway");
        }

        public class AreaData
        {
            public bool LightsOn = false;
            public AreaItemData ItemData;
        }
    }
}
