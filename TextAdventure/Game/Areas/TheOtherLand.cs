﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextAdventure.Game.Areas
{
    public class TheOtherLand : BaseSystem.Base.Area
    {
        public override string GetLookDescription()
        {
            return "An open empty space. There's nothing as far as the eye can see.";
        }

        public override void Load()
        {
            if (DataSaver.FileExists("TheOtherLand"))
            {
                TheOtherLandSave save = DataSaver.LoadFile<TheOtherLandSave>("TheOtherLand");

                ApplyItemSaveData(save.ItemData);
            }
        }

        public override void Save()
        {
            TheOtherLandSave save = new TheOtherLandSave();

            save.ItemData = GetItemSaveData();

            save.SaveAsFile("TheOtherLand");
        }

        public override void SetupConnections()
        {
            ConnectTo("Back to the test area", MainWindow.Instance.AreaHandler.FindAreaOfType<TestArea>(), "This leads back to the test area, where the house is.");
        }

        public class TheOtherLandSave
        {
            public AreaItemData ItemData;
        }
    }
}
