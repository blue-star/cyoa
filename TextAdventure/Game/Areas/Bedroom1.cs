﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TextAdventure.BaseSystem.Base;

namespace TextAdventure.Game.Areas
{
    public class Bedroom1 : BaseSystem.Base.Area
    {
        public override string GetLookDescription()
        {
            return "A small bedroom. There's a children's bed, and a small desk, along with a chair.";
        }

        public override void Load()
        {
            if (DataSaver.FileExists("Bedroom1"))
            {
                AreaData save = DataSaver.LoadFile<AreaData>("Bedroom1");

                ApplyItemSaveData(save.ItemData);
            }
        }

        public override void Save()
        {
            AreaData save = new AreaData();

            save.ItemData = GetItemSaveData();

            save.SaveAsFile("Bedroom1");
        }

        public override void SetupConnections()
        {
            ConnectTo("Hallway", MainWindow.Instance.AreaHandler.FindAreaOfType<UpperHallway>(), "Back to the hallway");
        }

        public class AreaData
        {
            public AreaItemData ItemData;
        }
    }
}
