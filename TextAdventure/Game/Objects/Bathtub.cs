﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TextAdventure.BaseSystem.Base;
using TextAdventure.Game.Areas;
using TextAdventure.Utility;

namespace TextAdventure.Game.Objects
{
    public class Bathtub : BaseSystem.Base.Item
    {
        public bool DidBreak
        {
            get
            {
                return SaveData.GetBool("didBreak");
            }

            set
            {
                SaveData.SetBool("didBreak", value);
            }
        }

        public override void DoActivateItem()
        {
            if (DidBreak)
            {
                MainWindow.Instance.WriteLineExamine("The faucet is broken, so you can't turn it.");
            }
            else
            {
                DidBreak = true;
                MainWindow.Instance.WriteLineExamine("You turn the faucet, but it breaks off and falls into the tub.");

                CurrentArea.Items.Add(new Faucet());
                MainWindow.Instance.RefreshAll();
            }
        }

        public override void DoExamineDescription()
        {
            if (DidBreak)
            {
                MainWindow.Instance.WriteLineExamine("A bathtub. It looks worn, and the faucet is broken.");
            }
            else
            {
                MainWindow.Instance.WriteLineExamine("A bathtub. It looks worn, but it seems to be fully intact.");
            }
        }

        public override bool GetCanActivate()
        {
            return true;
        }

        public override bool GetCanDrop()
        {
            return false;
        }

        public override bool GetCanPickUp()
        {
            return false;
        }
        
        public override string GetName()
        {
            return "Bathtub";
        }

        public override string GetShortDescription()
        {
            return "A white bathtub.";
        }

        public override void UseOnItem(Item other)
        {
            MainWindow.Instance.WriteLine("Nothing really seems to happen.");
        }
    }
}
