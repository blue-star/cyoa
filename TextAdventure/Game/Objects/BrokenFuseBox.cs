﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TextAdventure.BaseSystem.Base;
using TextAdventure.Utility;

namespace TextAdventure.Game.Objects
{
    public class BrokenFuseBox : BaseSystem.Base.Item
    {
        public override void DoActivateItem()
        {
            MainWindow.Instance.WriteLineExamine("As you prod your finger into it, the broken fuse sparks and hurts your hand.");
        }

        public override void DoExamineDescription()
        {
            MainWindow.Instance.WriteLineExamine("A fuse box. Something seems to have damaged it, so it's not working right now.");
        }

        public override bool GetCanActivate()
        {
            return true;
        }

        public override bool GetCanDrop()
        {
            return false;
        }

        public override bool GetCanPickUp()
        {
            return false;
        }
        
        public override string GetName()
        {
            return "Fuse Box";
        }

        public override string GetShortDescription()
        {
            return "A Fuse Box. It's broken.";
        }

        public override void UseOnItem(Item other)
        {
            MainWindow.Instance.WriteLine("Nothing really seems to happen.");
        }
    }
}
