﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TextAdventure.BaseSystem.Base;
using TextAdventure.Utility;

namespace TextAdventure.Game.Objects
{
    public class Screwdriver : BaseSystem.Base.Item
    {
        public override void DoActivateItem()
        {
            MainWindow.Instance.WriteLineExamine("Nothing seems to happen.");
        }

        public override void DoExamineDescription()
        {
            MainWindow.Instance.WriteLineExamine("A simple screwdriver with a yellow transparent handle.");
        }

        public override bool GetCanActivate()
        {
            return false;
        }

        public override bool GetCanDrop()
        {
            return true;
        }

        public override bool GetCanPickUp()
        {
            return true;
        }

        public override string GetName()
        {
            return "Screwdriver";
        }

        public override string GetShortDescription()
        {
            return "It's a screwdriver.";
        }

        public override void UseOnItem(Item other)
        {
            if(other is Screwdriver)
            {
                MainWindow.Instance.WriteLine("The first screwdriver seems to scratch the second.");
            }

            if(other is BrokenFuseBox)
            {
                MainWindow.Instance.WriteLine("You prod the Screwdriver into the broken fuse box. Many sparks fly out, and your hand is electrocuted. You drop the Screwdriver.");

                CurrentArea.Items.Add(new Screwdriver());
                DestroyItem();
            }

            MainWindow.Instance.WriteLine("Nothing really seems to happen.");
        }
    }
}
