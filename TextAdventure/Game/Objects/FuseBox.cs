﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TextAdventure.BaseSystem.Base;
using TextAdventure.Utility;

namespace TextAdventure.Game.Objects
{
    public class FuseBox : BaseSystem.Base.Item
    {
        public override void DoActivateItem()
        {
            MainWindow.Instance.WriteLineExamine("The fusebox seems to be working already.");
        }

        public override void DoExamineDescription()
        {
            MainWindow.Instance.WriteLineExamine("A fuse box. Something seems to have damaged it, but you wedged the Faucet in the electrical bits, so it seems to be working. Probably a fire hazard.");
        }

        public override bool GetCanActivate()
        {
            return true;
        }

        public override bool GetCanDrop()
        {
            return false;
        }

        public override bool GetCanPickUp()
        {
            return false;
        }
        
        public override string GetName()
        {
            return "Fuse Box";
        }

        public override string GetShortDescription()
        {
            return "A Fuse Box. A Faucet is wedged into it.";
        }

        public override void UseOnItem(Item other)
        {
            MainWindow.Instance.WriteLine("Nothing really seems to happen.");
        }
    }
}
