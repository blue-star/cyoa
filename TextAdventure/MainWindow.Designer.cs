﻿namespace TextAdventure
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.TextBox = new System.Windows.Forms.TextBox();
            this.ItemList = new System.Windows.Forms.ListBox();
            this.ItemDescription = new System.Windows.Forms.TextBox();
            this.ExamineItemBtn = new System.Windows.Forms.Button();
            this.ActivateBtn = new System.Windows.Forms.Button();
            this.UseWithBtn = new System.Windows.Forms.Button();
            this.PickUpDropBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.People = new System.Windows.Forms.ListBox();
            this.ExaminePersonBtn = new System.Windows.Forms.Button();
            this.InteractPersonBtn = new System.Windows.Forms.Button();
            this.TalkToBtn = new System.Windows.Forms.Button();
            this.PersonDescription = new System.Windows.Forms.TextBox();
            this.Directions = new System.Windows.Forms.ListBox();
            this.GoToPlace = new System.Windows.Forms.Button();
            this.ExamineDirection = new System.Windows.Forms.Button();
            this.GiveItem = new System.Windows.Forms.Button();
            this.Look = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.itemTabs = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.itemTabs.SuspendLayout();
            this.SuspendLayout();
            // 
            // TextBox
            // 
            this.TextBox.AcceptsReturn = true;
            this.TextBox.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.TextBox.Location = new System.Drawing.Point(178, 12);
            this.TextBox.Multiline = true;
            this.TextBox.Name = "TextBox";
            this.TextBox.ReadOnly = true;
            this.TextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TextBox.Size = new System.Drawing.Size(560, 547);
            this.TextBox.TabIndex = 0;
            // 
            // ItemList
            // 
            this.ItemList.FormattingEnabled = true;
            this.ItemList.Location = new System.Drawing.Point(745, 195);
            this.ItemList.Name = "ItemList";
            this.ItemList.Size = new System.Drawing.Size(159, 394);
            this.ItemList.Sorted = true;
            this.ItemList.TabIndex = 1;
            this.ItemList.SelectedIndexChanged += new System.EventHandler(this.ItemList_SelectedIndexChanged);
            // 
            // ItemDescription
            // 
            this.ItemDescription.Location = new System.Drawing.Point(744, 31);
            this.ItemDescription.Multiline = true;
            this.ItemDescription.Name = "ItemDescription";
            this.ItemDescription.ReadOnly = true;
            this.ItemDescription.Size = new System.Drawing.Size(159, 78);
            this.ItemDescription.TabIndex = 2;
            // 
            // ExamineItemBtn
            // 
            this.ExamineItemBtn.Location = new System.Drawing.Point(744, 116);
            this.ExamineItemBtn.Name = "ExamineItemBtn";
            this.ExamineItemBtn.Size = new System.Drawing.Size(75, 23);
            this.ExamineItemBtn.TabIndex = 5;
            this.ExamineItemBtn.Text = "Examine";
            this.ExamineItemBtn.UseVisualStyleBackColor = true;
            this.ExamineItemBtn.Click += new System.EventHandler(this.ExamineItemBtn_Click);
            // 
            // ActivateBtn
            // 
            this.ActivateBtn.Location = new System.Drawing.Point(828, 115);
            this.ActivateBtn.Name = "ActivateBtn";
            this.ActivateBtn.Size = new System.Drawing.Size(75, 23);
            this.ActivateBtn.TabIndex = 6;
            this.ActivateBtn.Text = "Activate";
            this.ActivateBtn.UseVisualStyleBackColor = true;
            this.ActivateBtn.Click += new System.EventHandler(this.ActivateBtn_Click);
            // 
            // UseWithBtn
            // 
            this.UseWithBtn.Location = new System.Drawing.Point(743, 145);
            this.UseWithBtn.Name = "UseWithBtn";
            this.UseWithBtn.Size = new System.Drawing.Size(75, 23);
            this.UseWithBtn.TabIndex = 7;
            this.UseWithBtn.Text = "Use With...";
            this.UseWithBtn.UseVisualStyleBackColor = true;
            this.UseWithBtn.Click += new System.EventHandler(this.UseWithBtn_Click);
            // 
            // PickUpDropBtn
            // 
            this.PickUpDropBtn.Location = new System.Drawing.Point(828, 145);
            this.PickUpDropBtn.Name = "PickUpDropBtn";
            this.PickUpDropBtn.Size = new System.Drawing.Size(75, 23);
            this.PickUpDropBtn.TabIndex = 8;
            this.PickUpDropBtn.Text = "Drop";
            this.PickUpDropBtn.UseVisualStyleBackColor = true;
            this.PickUpDropBtn.Click += new System.EventHandler(this.PickUpDropBtn_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(744, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(159, 16);
            this.label1.TabIndex = 9;
            this.label1.Text = "ITEMS";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // People
            // 
            this.People.FormattingEnabled = true;
            this.People.Location = new System.Drawing.Point(13, 351);
            this.People.Name = "People";
            this.People.Size = new System.Drawing.Size(159, 238);
            this.People.Sorted = true;
            this.People.TabIndex = 10;
            this.People.SelectedIndexChanged += new System.EventHandler(this.People_SelectedIndexChanged);
            // 
            // ExaminePersonBtn
            // 
            this.ExaminePersonBtn.Location = new System.Drawing.Point(12, 322);
            this.ExaminePersonBtn.Name = "ExaminePersonBtn";
            this.ExaminePersonBtn.Size = new System.Drawing.Size(75, 23);
            this.ExaminePersonBtn.TabIndex = 11;
            this.ExaminePersonBtn.Text = "Examine";
            this.ExaminePersonBtn.UseVisualStyleBackColor = true;
            this.ExaminePersonBtn.Click += new System.EventHandler(this.ExaminePersonBtn_Click);
            // 
            // InteractPersonBtn
            // 
            this.InteractPersonBtn.Location = new System.Drawing.Point(96, 322);
            this.InteractPersonBtn.Name = "InteractPersonBtn";
            this.InteractPersonBtn.Size = new System.Drawing.Size(75, 23);
            this.InteractPersonBtn.TabIndex = 12;
            this.InteractPersonBtn.Text = "Use item...";
            this.InteractPersonBtn.UseVisualStyleBackColor = true;
            this.InteractPersonBtn.Click += new System.EventHandler(this.InteractPersonBtn_Click);
            // 
            // TalkToBtn
            // 
            this.TalkToBtn.Location = new System.Drawing.Point(12, 293);
            this.TalkToBtn.Name = "TalkToBtn";
            this.TalkToBtn.Size = new System.Drawing.Size(75, 23);
            this.TalkToBtn.TabIndex = 13;
            this.TalkToBtn.Text = "Talk";
            this.TalkToBtn.UseVisualStyleBackColor = true;
            this.TalkToBtn.Click += new System.EventHandler(this.TalkToBtn_Click);
            // 
            // PersonDescription
            // 
            this.PersonDescription.Location = new System.Drawing.Point(12, 221);
            this.PersonDescription.Multiline = true;
            this.PersonDescription.Name = "PersonDescription";
            this.PersonDescription.ReadOnly = true;
            this.PersonDescription.Size = new System.Drawing.Size(159, 66);
            this.PersonDescription.TabIndex = 14;
            // 
            // Directions
            // 
            this.Directions.FormattingEnabled = true;
            this.Directions.Location = new System.Drawing.Point(13, 32);
            this.Directions.Name = "Directions";
            this.Directions.Size = new System.Drawing.Size(159, 121);
            this.Directions.Sorted = true;
            this.Directions.TabIndex = 15;
            this.Directions.SelectedIndexChanged += new System.EventHandler(this.Directions_SelectedIndexChanged);
            // 
            // GoToPlace
            // 
            this.GoToPlace.Location = new System.Drawing.Point(13, 159);
            this.GoToPlace.Name = "GoToPlace";
            this.GoToPlace.Size = new System.Drawing.Size(75, 23);
            this.GoToPlace.TabIndex = 16;
            this.GoToPlace.Text = "Go To";
            this.GoToPlace.UseVisualStyleBackColor = true;
            this.GoToPlace.Click += new System.EventHandler(this.GoToPlace_Click);
            // 
            // ExamineDirection
            // 
            this.ExamineDirection.Location = new System.Drawing.Point(97, 159);
            this.ExamineDirection.Name = "ExamineDirection";
            this.ExamineDirection.Size = new System.Drawing.Size(75, 23);
            this.ExamineDirection.TabIndex = 17;
            this.ExamineDirection.Text = "Examine";
            this.ExamineDirection.UseVisualStyleBackColor = true;
            this.ExamineDirection.Click += new System.EventHandler(this.ExamineDirection_Click);
            // 
            // GiveItem
            // 
            this.GiveItem.Location = new System.Drawing.Point(96, 293);
            this.GiveItem.Name = "GiveItem";
            this.GiveItem.Size = new System.Drawing.Size(75, 23);
            this.GiveItem.TabIndex = 18;
            this.GiveItem.Text = "Give item...";
            this.GiveItem.UseVisualStyleBackColor = true;
            this.GiveItem.Click += new System.EventHandler(this.GiveItem_Click);
            // 
            // Look
            // 
            this.Look.Location = new System.Drawing.Point(178, 565);
            this.Look.Name = "Look";
            this.Look.Size = new System.Drawing.Size(560, 23);
            this.Look.TabIndex = 19;
            this.Look.Text = "Look";
            this.Look.UseVisualStyleBackColor = true;
            this.Look.Click += new System.EventHandler(this.Look_Click);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(13, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(159, 16);
            this.label2.TabIndex = 20;
            this.label2.Text = "DIRECTIONS";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(10, 202);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(159, 16);
            this.label3.TabIndex = 21;
            this.label3.Text = "PEOPLE";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // itemTabs
            // 
            this.itemTabs.Controls.Add(this.tabPage1);
            this.itemTabs.Controls.Add(this.tabPage2);
            this.itemTabs.Location = new System.Drawing.Point(745, 174);
            this.itemTabs.Name = "itemTabs";
            this.itemTabs.SelectedIndex = 0;
            this.itemTabs.Size = new System.Drawing.Size(158, 414);
            this.itemTabs.TabIndex = 22;
            this.itemTabs.Selected += new System.Windows.Forms.TabControlEventHandler(this.itemTabs_Selected);
            // 
            // tabPage1
            // 
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(150, 388);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Inventory";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(150, 388);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "World";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(915, 600);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Look);
            this.Controls.Add(this.GiveItem);
            this.Controls.Add(this.ExamineDirection);
            this.Controls.Add(this.GoToPlace);
            this.Controls.Add(this.Directions);
            this.Controls.Add(this.PersonDescription);
            this.Controls.Add(this.TalkToBtn);
            this.Controls.Add(this.InteractPersonBtn);
            this.Controls.Add(this.ExaminePersonBtn);
            this.Controls.Add(this.People);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.PickUpDropBtn);
            this.Controls.Add(this.UseWithBtn);
            this.Controls.Add(this.ActivateBtn);
            this.Controls.Add(this.ExamineItemBtn);
            this.Controls.Add(this.ItemDescription);
            this.Controls.Add(this.ItemList);
            this.Controls.Add(this.TextBox);
            this.Controls.Add(this.itemTabs);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainWindow";
            this.Text = "Adventure!";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.itemTabs.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TextBox;
        private System.Windows.Forms.ListBox ItemList;
        private System.Windows.Forms.TextBox ItemDescription;
        private System.Windows.Forms.Button ExamineItemBtn;
        private System.Windows.Forms.Button ActivateBtn;
        private System.Windows.Forms.Button UseWithBtn;
        private System.Windows.Forms.Button PickUpDropBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox People;
        private System.Windows.Forms.Button ExaminePersonBtn;
        private System.Windows.Forms.Button InteractPersonBtn;
        private System.Windows.Forms.Button TalkToBtn;
        private System.Windows.Forms.TextBox PersonDescription;
        private System.Windows.Forms.ListBox Directions;
        private System.Windows.Forms.Button GoToPlace;
        private System.Windows.Forms.Button ExamineDirection;
        private System.Windows.Forms.Button GiveItem;
        private System.Windows.Forms.Button Look;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabControl itemTabs;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ToolTip toolTip;
    }
}

